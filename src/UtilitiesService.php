<?php

namespace Drupal\commerce_purolator_shipping_service;

use Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod\Purolator;
use Drupal\commerce_store\Entity\StoreInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Class UtilitiesService.
 *
 * Contains helper functions for the Purolator module.
 *
 * @package Drupal\commerce_purolator_shipping_service
 */
class UtilitiesService {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a UtilitiesService class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Build the form fields for the Canada Post API settings.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   A store entity, if the api settings are for a store.
   * @param \Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod\Purolator $shipping_method
   *   The Canada Post shipping method.
   *
   * @return array
   *   An array of form fields.
   *
   * @see \commerce_commerce_purolator_shipping_service_form_alter()
   */
  public function buildApiForm(StoreInterface $store = NULL, Purolator $shipping_method = NULL) {
    $form = [];

    // Fetch the Purolator API settings.
    $api_settings = $this->getApiSettings($store, $shipping_method);

    $form['commerce_purolator_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Purolator API authentication'),
      '#open' => FALSE,
    ];

    // If we are in the store form, display an option to set store-wide
    // settings.
    $checkbox_default_value = 0;
    if ($store) {
      $title = $this->t('Set store-wide Purolator API settings');
      $description = $this->t('The API settings defined here will be used by @url that do not define their own API settings.
        <br>Leave this box unchecked if your store does not use Purolator, or if you will be defining the API settings for each individual shipping method.', [
          '@url' => Link::fromTextAndUrl(
            $this->t('shipping methods'),
            Url::fromRoute('entity.commerce_shipping_method.collection')
          )->toString(),
        ]
      );

      // If the API settings are set on the store, set the default value to 1.
      if (!empty($store->get('purolator_api_settings')->getValue()[0]['value'])) {
        $checkbox_default_value = 1;
      }
    }
    // Else, if we are in the shipping method page display an option to set
    // shipping method settings.
    else {
      $title = $this->t('Override Purolator store API settings');
      $description = $this->t("Leave this box unchecked if you'd like to use @url when fetching rates and tracking details.", [
        '@url' => Link::fromTextAndUrl(
          $this->t('the store API settings'),
          Url::fromRoute('entity.commerce_store.collection')
        )->toString(),
      ]);

      // If the API settings are set on the method, set the default value to 1.
      if ($shipping_method->apiIsConfigured()) {
        $checkbox_default_value = 1;
      }
    }

    $form['commerce_purolator_api']['store_settings'] = [
      '#type' => 'checkbox',
      '#title' => $title,
      '#description' => $description,
      '#default_value' => $checkbox_default_value,
    ];

    $form['commerce_purolator_api']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Purolator Key'),
      '#default_value' => $api_settings ? $api_settings['key'] : '',
      '#required' => TRUE,
      '#description' => $this->t('Your Purolator key. Visit https://eship.purolator.com/site/default.aspx to get one.'),
    ];
    $form['commerce_purolator_api']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Purolator Key Password'),
      '#default_value' => $api_settings ? $api_settings['password'] : '',
      '#required' => TRUE,
      '#description' => $this->t('Your Purolator account key password. Visit https://eship.purolator.com/site/default.aspx to get one.'),
    ];
    $form['commerce_purolator_api']['account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Purolator Account Number'),
      '#default_value' => $api_settings ? $api_settings['account_number'] : '',
      '#required' => TRUE,
      '#description' => $this->t('Your Purolator account number. Visit https://eship.purolator.com/site/default.aspx to get one.'),
    ];
    $form['commerce_purolator_api']['from_postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship from postal code'),
      '#default_value' => $api_settings ? $api_settings['from_postal_code'] : '',
      '#description' => $this->t('Postal code to ship from.'),
    ];
    $form['commerce_purolator_api']['from_country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship from country code'),
      '#default_value' => $api_settings ? $api_settings['from_postal_code'] : '',
      '#description' => $this->t('Postal code to ship from.'),
    ];
    $form['commerce_purolator_api']['from_country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship from country code'),
      '#default_value' => $api_settings ? $api_settings['from_country_code'] : '',
      '#description' => $this->t('Country Code to ship from. Example being CA for Canada.'),
    ];
    $form['commerce_purolator_api']['from_province'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship from province'),
      '#default_value' => $api_settings ? $api_settings['from_province'] : '',
      '#description' => $this->t('Province to ship from. Example being ON for Ontario.'),
    ];
    $form['commerce_purolator_api']['from_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ship from city'),
      '#default_value' => $api_settings ? $api_settings['from_city'] : '',
      '#description' => $this->t('City to ship from. Uses the full city name. Example being Ottawa for Ottawa.'),
    ];
    $form['commerce_purolator_api']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#default_value' => $api_settings ? $api_settings['mode'] : '',
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#required' => TRUE,
    ];
    $form['commerce_purolator_api']['log'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Log the following messages for debugging'),
      '#options' => [
        'request' => $this->t('API request messages'),
        'response' => $this->t('API response messages'),
      ],
      '#default_value' => $api_settings ? $api_settings['log'] : [],
    ];

    $this->alterApiFormFields($form, $store);

    return $form;
  }

  /**
   * Decode the Purolator API settings stored as json in the store entity.
   *
   * @param object $api_settings
   *   The json encoded Canada Post api settings.
   *
   * @return array
   *   An array of values extracted from the json object.
   */
  public function decodeSettings($api_settings) {
    return json_decode($api_settings, TRUE);
  }

  /**
   * Encode the Canada Post API settings values in a json object.
   *
   * @param array $values
   *   The form_state values with the Canada Post API settings.
   *
   * @return object
   *   The encoded json object.
   */
  public function encodeSettings(array $values) {
    foreach ($this->getApiKeys() as $key) {
      $api_settings_values[$key] = $values[$key];
    }

    return json_encode($api_settings_values);
  }

  /**
   * Return the Canada Post API keys.
   *
   * @return array
   *   An array of API setting keys.
   */
  public function getApiKeys() {
    return [
      'key',
      'password',
      'account_number',
      'from_postal_code',
      'from_country_code',
      'from_province',
      'from_city',
      'mode',
      'log',
    ];
  }

  /**
   * Fetch the Purolator API settings, first from the method, then, the store.
   *
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   A store entity, if the api settings are for a store.
   * @param \Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod\Purolator $shipping_method
   *   The shipping method.
   *
   * @throws \Exception
   *
   * @return array
   *   Returns the api settings.
   */
  public function getApiSettings(
    StoreInterface $store = NULL,
    Purolator $shipping_method = NULL
  ) {
    $api_settings = [];

    if (!$store && !$shipping_method) {
      throw new Exception('A shipping method or a store is required to fetch the Purolator API settings.');
    }

    // Check if we have settings set on the shipping method, if so, use that.
    if ($shipping_method && $shipping_method->apiIsConfigured()) {
      $api_settings = $shipping_method->getConfiguration()['api'];
    }
    // Else, we fallback to the store API settings.
    elseif ($store && !empty($store->get('purolator_api_settings')->getValue()[0]['value'])) {
      $api_settings = $this->decodeSettings(
        $store->get('purolator_api_settings')->getValue()[0]['value']
      );
    }

    return $api_settings;
  }

  /**
   * Alter the Purolator API settings form fields.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\commerce_store\Entity\StoreInterface $store
   *   A store entity, if the api settings are for a store.
   */
  protected function alterApiFormFields(array &$form, StoreInterface $store = NULL) {
    // Fields should be visible only if the store_settings checkbox is checked.
    // The input name is different in the shipping method form, so detect where
    // we are and change accordingly.
    $field_input_name = 'commerce_purolator_store_settings';
    if (!$store) {
      $field_input_name = 'plugin[0][target_plugin_configuration][purolator][commerce_purolator_api][commerce_purolator_store_settings]';
    }

    $states = [
      'visible' => [
        ':input[name="' . $field_input_name . '"]' => [
          'checked' => TRUE,
        ],
      ],
      'required' => [
        ':input[name="' . $field_input_name . '"]' => [
          'checked' => TRUE,
        ],
      ],
    ];
    foreach ($this->getApiKeys() as $key) {
      $form['commerce_purolator_api']["$key"]['#states'] = $states;
      $form['commerce_purolator_api']["$key"]['#required'] = FALSE;
    }

    // Log is not required so remove it from the states as
    // well.
    unset($form['commerce_purolator_api']['log']['#states']['required']);
  }

}
