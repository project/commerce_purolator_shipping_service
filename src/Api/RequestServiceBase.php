<?php

namespace Drupal\commerce_purolator_shipping_service\Api;

use Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod\Purolator;
use Drupal\commerce_purolator_shipping_service\UtilitiesService;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_store\Entity\StoreInterface;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Purolator API Service.
 *
 * @package Drupal\commerce_purolator_shipping_service
 */
abstract class RequestServiceBase {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The utilities service class.
   *
   * @var \Drupal\commerce_purolator_shipping_service\UtilitiesService
   */
  protected $utilities;

  /**
   * RequestServiceBase class constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\commerce_purolator_shipping_service\UtilitiesService $utilities
   *   The utilities service class.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    UtilitiesService $utilities
  ) {
    $this->logger = $logger_factory->get(COMMERCE_PUROLATOR_LOGGER_CHANNEL);
    $this->utilities = $utilities;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiSettings(
    StoreInterface $store = NULL,
    Purolator $shipping_method = NULL
    ) {
    return $this->utilities->getApiSettings($store, $shipping_method);
  }

  /**
   * Returns the request parameter data required for purolator SOAP request.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   * @param array $api_settings
   *   The Purolator API settings.
   *
   * @return string
   *   The environment mode (prod/dev).
   */
  public function getPurolatorRequestParams(
    ShipmentInterface $shipment,
    array $api_settings
    ) {
    $request = new \stdClass();
    $request->ReceiverAddress = new \stdClass();
    $request->TotalWeight = new \stdClass();

    // Populate the Origin Information.
    $request->SenderPostalCode = $api_settings['from_postal_code'];

    // Populate the destination Information.
    $address = $shipment->getShippingProfile()->get('address')->first();

    $request->ReceiverAddress->City = $address->getLocality();
    $request->ReceiverAddress->Province = $address->getAdministrativeArea();
    $request->ReceiverAddress->Country = $address->getCountryCode();
    $request->ReceiverAddress->PostalCode = $address->getPostalCode();
    $request->BillingAccountNumber = $api_settings['account_number'];

    $request->PackageType = "CustomerPackaging";
    $weight = $shipment->getWeight()->convert('kg')->getNumber();
    $request->TotalWeight->Value = ($weight < 1) ? 1 : $weight;
    $request->TotalWeight->WeightUnit = 'kg';

    return $request;
  }

}
