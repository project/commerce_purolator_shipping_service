<?php

namespace Drupal\commerce_purolator_shipping_service\Api;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;

/**
 * Provides the default Rating API integration services.
 */
class RatingService extends RequestServiceBase implements RatingServiceInterface {

  /**
   * {@inheritdoc}
   */
  public function getRates(
    ShippingMethodInterface $shipping_method,
    ShipmentInterface $shipment
  ) {
    $order = $shipment->getOrder();
    $store = $order->getStore();
    $api_settings = $this->utilities
      ->getApiSettings(
        $store,
        $shipping_method->getPlugin()
      );

    // Get the request object required for purolator soap request.
    $request = (object) $this->getPurolatorRequestParams(
      $shipment,
      $api_settings
    );

    // Log request if it is configured to log them.
    if ($api_settings['log']['request']) {
      $log_output = var_export($request, TRUE);
      $message = sprintf(
        'Rating request made for order "%s". Request object: "%s".',
        $order->id(),
        $log_output
      );
      $this->logger->info($message);
    }

    $client = $this->getClient($api_settings);
    $response = $client->GetQuickEstimate($request);

    // Log response if it is configured to log them.
    if ($api_settings['log']['response']) {
      $log_output = var_export($response, TRUE);
      $message = sprintf(
          'Rating request made for order "%s". Response received: "%s".',
          $order->id(),
          $log_output
        );
      $this->logger->info($message);
    }

    $rates = $this->parseResponse(
        $shipping_method,
        $response,
        $order->id()
      );

    $enabled_services = $shipping_method->getPlugin()->getServices();
    foreach ($rates as $key => $rate) {
      $rate_id = $rate->getId();
      $id_array = explode('--', $rate_id);
      $returned_service = end($id_array);
      if (!array_key_exists($returned_service, $enabled_services)) {
        unset($rates[$key]);
      }
    }

    if (is_array($rates)) {
      return $rates;
    }

    return [];
  }

  /**
   * Creates a SOAP Client in Non-WSDL mode for purolator.
   */
  private function getClient(array $api_settings) {
    $location = "https://webservices.purolator.com/EWS/V2/Estimating/EstimatingService.asmx";
    $wsdl_url = "https://webservices.purolator.com/EWS/V2/Estimating/EstimatingService.asmx?wsdl";
    if ($api_settings['mode'] == 'test') {
      $location = "https://devwebservices.purolator.com/EWS/V2/Estimating/EstimatingService.asmx";
      $wsdl_url = "https://devwebservices.purolator.com/EWS/V2/Estimating/EstimatingService.wsdl";
    }

    // Set the parameters for the Non-WSDL mode SOAP communication with
    // proper credentials.
    $client = new \SoapClient(
      $wsdl_url,
      [
        'trace' => TRUE,
        'location' => $location,
        'uri' => "http://purolator.com/pws/datatypes/v2",
        'login' => $api_settings['key'],
        'password' => $api_settings['password'],
      ]
      );

    // Define the SOAP Envelope Headers.
    $headers[] = new \SoapHeader(
      'http://purolator.com/pws/datatypes/v2',
      'RequestContext',
      [
        'Version' => '2.0',
        'Language' => 'en',
        'GroupID' => 'xxx',
        'RequestReference' => 'Rating Example',
      ]
      );

    // Apply the SOAP Header to your client.
    $client->__setSoapHeaders($headers);

    return $client;
  }

  /**
   * Parse the response from puroator and return shipping rates.
   *
   * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method
   *   The shipping method.
   * @param object $response
   *   The purolator response object.
   * @param string $order_id
   *   The order id.
   *
   * @return array|void
   *   The shipping rates array.
   */
  private function parseResponse(
    ShippingMethodInterface $shipping_method,
    $response,
    $order_id
  ) {
    $rates = [];
    if (!empty($response->ResponseInformation->Errors->Error)) {
      // Log the error message.
      $message = sprintf(
          'Error recieved when requesting Rating for order "%s". Response received: "%s".',
          $order_id,
          $response->ResponseInformation->Errors->Error->Description
        );
      $this->logger->info($message);

      return $response->ResponseInformation->Errors->Error->Description;
    }

    if (!$response->ShipmentEstimates->ShipmentEstimate) {
      // Log the error message.
      $message = sprintf(
        'No rate data recieved for order "%s".',
        $order_id
      );
      $this->logger->info($message);

      return $rates;
    }

    foreach ($response->ShipmentEstimates->ShipmentEstimate as $rate) {
      $price = new Price((string) $rate->TotalPrice, 'CAD');

      $rates[] = new ShippingRate([
        'shipping_method_id' => $shipping_method->id(),
        'service' => new ShippingService($rate->ServiceID, $rate->ServiceID),
        'amount' => $price,
      ]);
    }

    return $rates;
  }

}
