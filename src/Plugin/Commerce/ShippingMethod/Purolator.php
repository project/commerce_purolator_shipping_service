<?php

namespace Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_purolator_shipping_service\UtilitiesService;
use Drupal\commerce_purolator_shipping_service\Api\RatingServiceInterface;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;

use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Purolator shipping method.
 *
 * @CommerceShippingMethod(
 *  id = "purolator",
 *  label = @Translation("Purolator"),
 *  services = {
 *    "PurolatorGround" = @Translation("Purolator Ground"),
 *    "PurolatorExpress" = @Translation("Purolator Express"),
 *    "PurolatorExpress9AM" = @Translation("Purolator Express 9AM"),
 *    "PurolatorExpress10:30AM" = @Translation("Purolator Express 10:30AM"),
 *    "PurolatorFree" = @Translation("Free Shipping"),
 *   }
 * )
 */
class Purolator extends ShippingMethodBase {

  /**
   * The Canada Post utilities service object.
   *
   * @var \Drupal\commerce_purolator_shipping_service\UtilitiesService
   */
  protected $utilities;

  /**
   * The rating service.
   *
   * @var \Drupal\commerce_purolator_shipping_service\Api\RatingServiceInterface
   */
  protected $ratingService;

  /**
   * Constructs a new Purolator object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_purolator_shipping_service\UtilitiesService $utilities
   *   Purolator utilities service object.
   * @param \Drupal\commerce_purolator_shipping_service\Api\RatingServiceInterface $rating_service
   *   The Purolator Rating service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    UtilitiesService $utilities,
    RatingServiceInterface $rating_service
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $package_type_manager,
      $workflow_manager
    );

    $this->utilities = $utilities;
    $this->ratingService = $rating_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_purolator_shipping_service.utilities_service'),
      $container->get('commerce_purolator_shipping_service.rating_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api' => [
        'key' => '',
        'password' => '',
        'account_number' => '',
        'from_postal_code' => '',
        'from_country_code' => '',
        'from_province' => '',
        'from_city' => '',
        'mode' => 'test',
        'log' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form += $this->utilities->buildApiForm(NULL, $this);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    // If the user didn't enter API settings for this method, make sure we have
    // settings in all the stores that this shipping method is available for.
    if ($values['commerce_purolator_api']['store_settings']) {
      return parent::validateConfigurationForm($form, $form_state);
    }

    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method */
    $shipping_method = $form_state->getFormObject()->getEntity();
    $stores = $shipping_method->getStores();

    foreach ($stores as $store) {
      // If settings are empty, output an error and break out of the loop.
      if (!empty($store->get('purolator_api_settings')->getValue()[0]['value'])) {
        continue;
      }

      $form_state->setErrorByName(
        'plugin][0][target_plugin_configuration][purolator][commerce_purolator_api][store_settings',
        $this->t('
          There are stores, which this shipping method is available for,
          that do not have Purolator API settings defined. Please configure
          the API settings for these stores or this shipping method.
        ')
      );
      break;
    }

    return parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    // Save the API settings if store settings checkbox is checked.
    if ($values['commerce_purolator_api']['store_settings']) {
      foreach ($this->utilities->getApiKeys() as $key) {
        $this->configuration['api'][$key] = $values['commerce_purolator_api']["$key"];
      }
    }

    return parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Determine if we have the minimum information to connect to Purolator.
   *
   * @return bool
   *   TRUE if there is enough information to connect, FALSE otherwise.
   */
  public function apiIsConfigured() {
    $api_information = $this->configuration['api'];

    return (
      !empty($api_information['key'])
      && !empty($api_information['password'])
      && !empty($api_information['account_number'])
      && !empty($api_information['mode'])
    );
  }

  /**
   * Calculates rates for the given shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return \Drupal\commerce_shipping\ShippingRate[]
   *   The rates.
   */
  public function calculateRates(ShipmentInterface $shipment) {
    // Only attempt to collect rates if an address exists on the shipment.
    if ($shipment->getShippingProfile()->get('address')->isEmpty()) {
      return [];
    }

    return $this->ratingService->getRates(
      $this->parentEntity,
      $shipment
    );
  }

}
