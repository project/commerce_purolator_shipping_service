What is Purolator?
Purolator E-Ship Web Services are easy-to-use and free of charge web components that enable you to integrate real-time
shipping, tracking, estimates, pickups and returns* directly into your website or custom application.
