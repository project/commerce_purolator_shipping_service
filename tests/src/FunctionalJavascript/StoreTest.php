<?php

namespace Drupal\Tests\commerce_purolator_shipping_service\FunctionalJavascript;

use Drupal\Tests\commerce_store\FunctionalJavascript\StoreTest as CoreStoreTest;

/**
 * Tests the store UI.
 *
 * @group commerce_purolator_shipping_service
 */
class StoreTest extends CoreStoreTest {

  /**
   * A store type entity to use in the tests.
   *
   * @var \Drupal\commerce_store\Entity\StoreTypeInterface
   */
  protected $type;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_purolator_shipping_service',
    'commerce_shipping',
    'commerce_shipping_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_shipping_method',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Tests creating a store without store wide purolator settings.
   */
  public function testCreateStoreWithoutPurolator() {
    $this->drupalGet('admin/commerce/config/stores');
    $this->getSession()->getPage()->clickLink('Add store');

    // Check the integrity of the form.
    $this->assertSession()->fieldExists('store_settings');
    $this->getSession()->getPage()->fillField('address[0][address][country_code]', 'US');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $name = $this->randomMachineName(8);
    $edit = [
      'name[0][value]' => $name,
      'mail[0][value]' => \Drupal::currentUser()->getEmail(),
      'default_currency' => 'USD',
      'timezone' => 'UTC',
    ];
    $address = [
      'address_line1' => '1098 Alta Ave',
      'locality' => 'Mountain View',
      'administrative_area' => 'CA',
      'postal_code' => '94043',
    ];
    foreach ($address as $property => $value) {
      $path = 'address[0][address][' . $property . ']';
      $edit[$path] = $value;
    }
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains("Saved the $name store.");
  }

  /**
   * Tests editing a store.
   */
  public function testEditStoreWithoutPurolator() {
    $store = $this->createStore('Test');

    $this->drupalGet($store->toUrl('edit-form'));
    $edit = [
      'name[0][value]' => 'Test!',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains("Saved the Test! store.");

    // Add a purolator shipping method.
    $this->drupalGet('admin/commerce/shipping-methods');
    $this->getSession()->getPage()->clickLink('Add shipping method');

    $this->assertSession()->addressEquals('admin/commerce/shipping-methods/add');
    $this->getSession()->getPage()->fillField('name[0][value]', 'Purolator');
    $this->getSession()->getPage()->fillField('plugin[0][target_plugin_id]', 'purolator');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $name = $this->randomMachineName(8);
    $edit = [
      'name[0][value]' => $name,
      'plugin[0][target_plugin_configuration][purolator][services][PurolatorGround]' => 'PurolatorGround',
    ];

    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains("Saved the $name shipping method.");
  }

}
