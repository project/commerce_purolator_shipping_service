<?php

namespace Drupal\Tests\commerce_purolator_shipping_service\Unit;

use Drupal\commerce_purolator_shipping_service\Plugin\Commerce\ShippingMethod\Purolator;
use Drupal\commerce_purolator_shipping_service\UtilitiesService;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Plugin\Commerce\PackageType\PackageTypeInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\physical\Length;
use Drupal\physical\Weight;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

use Drupal\Tests\UnitTestCase;

/**
 * Base class for purolator shipping method tests.
 *
 * @package Drupal\Tests\commerce_purolator_shipping_service\Unit
 */
abstract class PurolatorUnitTestBase extends UnitTestCase {

  /**
   * The shipping method interface.
   *
   * @var \Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface
   */
  protected $shippingMethod;

  /**
   * The shipment interface.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * The utilities service class.
   *
   * @var \Drupal\commerce_purolator_shipping_service\UtilitiesService
   */
  protected $utilities;

  /**
   * Set up requirements for test.
   */
  public function setUp(): void {
    parent::setUp();

    $this->shippingMethod = $this->mockShippingMethod();
    $this->shipment = $this->mockShipment();

    $utilities = $this->prophesize(UtilitiesService::class);
    $utilities
      ->getApiSettings(
        $this->shipment->getOrder()->getStore(),
        $this->shippingMethod
      )
      ->willReturn([]);
    $this->utilities = $utilities->reveal();
  }

  /**
   * Creates a mock Drupal Commerce shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   A mocked commerce shipment object.
   */
  public function mockShipment() {
    // Mock a Drupal Commerce Order and associated objects.
    $order = $this->prophesize(OrderInterface::class);
    $store = $this->prophesize(StoreInterface::class);

    // Mock the store API settings.
    $api_settings = $this->prophesize(TextLongItem::class);
    $encoded_api_settings = json_encode($this->getStoreApiSettings());
    $api_settings->getValue()->willReturn([
      0 => [
        'value' => $encoded_api_settings,
      ],
    ]);
    $store->get('purolator_api_settings')->willReturn($api_settings);

    // Mock a Drupal Commerce shipment and associated objects.
    $shipment = $this->prophesize(ShipmentInterface::class);
    $shipment->getOrder()->willReturn($order->reveal());
    $shipment->getShippingMethod()->willReturn($this->shippingMethod);

    $order->getStore()->willReturn($store->reveal());

    // Return the mocked shipment object.
    return $shipment->reveal();
  }

  /**
   * Creates a mock Drupal Commerce shipping method.
   *
   * @return \Drupal\commerce_canadapost\Plugin\Commerce\ShippingMethod\CanadaPost
   *   The mocked shipping method.
   */
  public function mockShippingMethod() {
    $shipping_method = $this->prophesize(Purolator::class);
    $package_type = $this->prophesize(PackageTypeInterface::class);
    $package_type->getHeight()->willReturn(new Length(10, 'in'));
    $package_type->getLength()->willReturn(new Length(10, 'in'));
    $package_type->getWidth()->willReturn(new Length(3, 'in'));
    $package_type->getWeight()->willReturn(new Weight(10, 'lb'));
    $package_type->getRemoteId()->willReturn('custom');
    $shipping_method->apiIsConfigured()->willReturn(TRUE);
    $shipping_method->getDefaultPackageType()->willReturn($package_type);
    $shipping_method->getConfiguration()->willReturn([
      'api' => $this->getShipmentMethodApiSettings(),
    ]);

    return $shipping_method->reveal();
  }

  /**
   * Returns an array of mock Purolator API settings.
   *
   * @return array
   *   The API settings.
   */
  protected function getShipmentMethodApiSettings() {
    return [
      'key' => 'shipment_method_mock_key',
      'password' => 'shipment_method_mock_pwd',
      'account_number' => 'shipment_method_mock_account_number',
      'from_postal_code' => 'shipment_method_mock_pin',
      'from_country_code' => 'CA',
      'from_province' => 'ON',
      'from_city' => 'Torronto',
      'mode' => 'test',
      'log' => [
        'request' => FALSE,
        'response' => FALSE,
      ],
    ];
  }

  /**
   * Returns an array of mock Purolator settings.
   *
   * @return array
   *   The API settings.
   */
  protected function getStoreApiSettings() {
    return [
      'key' => 'store_mock_key',
      'password' => 'store_mock_pwd',
      'account_number' => 'store_mock_account_number',
      'from_postal_code' => 'store_mock_pin',
      'from_country_code' => 'US',
      'from_province' => 'AL',
      'from_city' => 'California',
      'mode' => 'live',
      'log' => [
        'request' => FALSE,
        'response' => FALSE,
      ],
    ];
  }

}
