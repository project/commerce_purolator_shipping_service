<?php

namespace Drupal\Tests\commerce_purolator_shipping_service\Unit;

use Drupal\commerce_purolator_shipping_service\UtilitiesService;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Tests the purolator api settings form.
 *
 * @group commerce_purolator_shipping_service
 */
class PurolatorApiSettingsTest extends PurolatorUnitTestBase {

  /**
   * Purolator service object.
   *
   * @var \Drupal\commerce_purolator_shipping_service\UtilitiesService
   */
  protected $utilities;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->shippingMethod = $this->mockShippingMethod();
    $this->shipment = $this->mockShipment();

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->utilities = new UtilitiesService($entity_type_manager->reveal());
  }

  /**
   * Test the getApiSettings settings method for shipping method.
   */
  public function testShippingMethodApiSettings() {
    // Get the API settings w/o passing a store entity.
    $api_settings = $this->utilities->getApiSettings(NULL, $this->shippingMethod);

    // Now, test that we are returned back the shipment API settings.
    $this->assertEquals('shipment_method_mock_key', $api_settings['key']);
    $this->assertEquals('shipment_method_mock_pwd', $api_settings['password']);
    $this->assertEquals('shipment_method_mock_account_number', $api_settings['account_number']);
    $this->assertEquals('shipment_method_mock_pin', $api_settings['from_postal_code']);
    $this->assertEquals('CA', $api_settings['from_country_code']);
    $this->assertEquals('ON', $api_settings['from_province']);
    $this->assertEquals('Torronto', $api_settings['from_city']);
    $this->assertEquals('test', $api_settings['mode']);
  }

  /**
   * Test the getApiSettings settings method for store.
   */
  public function testStoreApiSettings() {
    // Get the API settings passing a store entity.
    $api_settings = $this->utilities->getApiSettings($this->shipment->getOrder()->getStore());

    // Now, test that we are returned back the store API settings.
    $this->assertEquals('store_mock_key', $api_settings['key']);
    $this->assertEquals('store_mock_pwd', $api_settings['password']);
    $this->assertEquals('store_mock_account_number', $api_settings['account_number']);
    $this->assertEquals('store_mock_pin', $api_settings['from_postal_code']);
    $this->assertEquals('US', $api_settings['from_country_code']);
    $this->assertEquals('AL', $api_settings['from_province']);
    $this->assertEquals('California', $api_settings['from_city']);
    $this->assertEquals('live', $api_settings['mode']);
  }

  /**
   * Test the getApiSettings settings method preference.
   */
  public function testShippingMethodHasPreference() {
    // Get the API settings passing a store entity and shipping method entity
    // and test that we get back the shipping method settings as that has
    // preference.
    $api_settings = $this->utilities
      ->getApiSettings($this->shipment->getOrder()->getStore(), $this->shippingMethod);

    // Now, test that we are returned back the store API settings.
    $this->assertEquals('shipment_method_mock_key', $api_settings['key']);
    $this->assertEquals('shipment_method_mock_pwd', $api_settings['password']);
    $this->assertEquals('shipment_method_mock_account_number', $api_settings['account_number']);
    $this->assertEquals('shipment_method_mock_pin', $api_settings['from_postal_code']);
    $this->assertEquals('CA', $api_settings['from_country_code']);
    $this->assertEquals('ON', $api_settings['from_province']);
    $this->assertEquals('Torronto', $api_settings['from_city']);
    $this->assertEquals('test', $api_settings['mode']);
  }

}
